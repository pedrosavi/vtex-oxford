'use strict';

module.exports = function (grunt) {

	var config,
   		environment,
    	errorHandler,
    	legacy,
    	name,
    	open,
    	pkg,
    	port,
    	taskArray,
    	taskName,
    	tasks,
    	verbose,
    	_results,
    	gruntBanner;

  	pkg = grunt.file.readJSON('package.json');

  	gruntBanner = '/**\n' +
        ' * Avanti Comunicação <dev@penseavanti.com.br>\n' +
        ' * '+ pkg.accountName +'\n' +
        ' * @date <%= grunt.template.today("dd/mm/yyyy HH:MM:ss") %>\n' +
        ' */\n';

  	verbose = grunt.option('verbose');

	/**
	* Set port
	*/
  	port = '';
  	if (pkg.localPort === 80) {
  		port = ':'+ pkg.localPort;
  	}

  	/**
   	* Set Environment
   	*/
  	environment = 'vtexcommercestable';
  	open = 'http://' + pkg.accountName + '.vtexlocal.com.br'+ port;

  	/**
   * Check open var set in package.json
   */
  	if (pkg.open === false) {
    	open = false;
  	}

  	errorHandler = function(err, req, res, next) {
   		var errString, _ref, _ref1;
    	errString = (_ref = (_ref1 = err.code) != null ? _ref1.red : void 0) != null ? _ref : err.toString().red;
    	return grunt.log.warn(errString, req.url.yellow);
  	};

  	config = {
		// Watch
		watch: {
			javascriptMerge: {
				files: ['assets/js/**/*.js'],
				tasks: ['uglify:dev']
		  	},

            javascript: {
                files: ['assets/js/*.js'],
                tasks: ['copy:javascript']
            },

		  	img: {
				files: ['assets/img/*','!assets/img/sprite/*.*'],
				tasks: ['copy:devImg']
		  	},

		  	scss: {
				files: 'assets/scss/**/*.scss',
				tasks: ['compass:dev','copy:devCss']
		  	},

            html: {
                files: 'assets/html/*',
                tasks: ['copy:html']
            },

            font: {
                files: 'assets/font/*',
                tasks: ['copy:font']
            }
		},

		// Limpa as pastas build e deploy
        clean: {
            dev: {
                src: ['build/*']
            },
            deploy: {
                src: ['deploy/*']
            }
        },

		// Copia arquivos
		copy: {
			devCss: {
		        files: [
		          	{
		          		expand: true,
		          		flatten: true,
		          		src: 'assets/css/*.css',
		          		dest: 'build/arquivos/'
		          	},

		          	//P
		          	{
		          		expand: true,
		          		flatten: true,
		          		src: 'temp/*.css',
		          		dest: 'build/arquivos/'
		          	}

		        ]
		    },
		    devImg: {
		        files: [
		          	{
		          		expand: true,
		          		flatten: true,
		          		src: 'assets/img/*.{png,jpg,gif}',
		          		dest: 'build/arquivos/'
		          	}
		        ]
		    },
            javascript: {
                files: [
                    {
                        expand: false,
                        flatten: true,
                        src: 'assets/js/*.js',
                        dest: 'build/arquivos/'
                    }
                ]
            },
            html: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: 'assets/html/*',
                        dest: 'build/html/'
                    }
                ]
            },
            font: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: 'assets/font/*',
                        dest: 'build/arquivos/'
                    }
                ]
            },
		    deploy: {
		        files: [
		        	{
		        		expand: true,
		        		flatten: true,
		        		src: 'build/arquivos/*.css',
		        		dest: 'deploy/css/'
		        	}
		        ]
		    }
		},

		// Reduz imagens
		imagemin: {
		    deploy: {
		    	files: [
		    		{
		        		expand: true,
		        		flatten: true,
        				src: 'build/arquivos/*.{png,jpg,gif}',
		        		dest: 'deploy/img/',
		      		}
		      	]
		    }
		},


		// Processa os arquivos .scss
		compass: {
			dev: {
				dist: {
			  		options: {
						force: true,
                        config: 'config.rb',
                        raw: 'Encoding.default_external = \'utf-8\'\n'
			  		}
				}
		  	},
		  	deploy: {
				dist: {
			  		options: {
						force: true,
                        config: 'config.rb',
                        banner: gruntBanner,
                        raw: 'Encoding.default_external = \'utf-8\'\n'
			  		}
				}
		  	}
		},

		// Processa os arquivos .js
		uglify: {
		  	dev: {
			  	options: {
					mangle: false,
					compress: false,
					beautify: false,
					preserveComments: 'all'
				},
				files: {
					'build/arquivos/0-oxf-web-application.js': [
                        'assets/js/lib/**/*.js',
                        'assets/js/core/Namespace.js',
			            'assets/js/core/Util.js',
			            'assets/js/core/Main.js',
                        'assets/js/component/*.js',
			            'assets/js/controller/*.js'
					]
				}
			},
			deploy: {
			 	options: {
					mangle: true,
					compress: {
				  		drop_console: true
					},
                    banner: gruntBanner
			  	},
				files: {
					'deploy/js/0-oxf-web-application.js': ['build/arquivos/0-oxf-web-application.js']
			  	}
		  	}
		},

        // Colocar o banner no css final
        usebanner: {
            options: {
                position: 'top',
                banner: gruntBanner,
                linebreak: true
            },
            files: {
                src: [
                    'deploy/css/0-oxf-web-style.css',
                    'deploy/css/checkout-custom.css',
                ]
            }
        },

		connect: {
	     	http: {
		        options: {
			        hostname: "*",
			        open: open,
			        port: pkg.localPort,
			        middleware: [
			            require('connect-livereload')({
			              disableCompression: true
			            }), require('connect-http-please')({
			              replaceHost: (function(h) {
			                return h.replace("vtexlocal", environment);
			              })
			            }, {
			              verbose: verbose
			            }), require('connect-tryfiles')('**', "http://portal." + environment + ".com.br:80", {
			              cwd: 'build/',
			              verbose: verbose
			            }), require('connect')["static"]('./build/'), errorHandler
			        ]
		        }
	      	}
	    }
	}

  	grunt.registerTask('default', [
		'clean:dev',
        'uglify:dev',
        'compass:dev',
        'copy:devCss',
        'copy:devImg',
        'copy:html',
        'copy:font',
        'connect',
		'watch'
    ]);

  	grunt.registerTask('deploy', [
        'clean:deploy',
		'uglify:deploy',
		'compass:deploy',
		'imagemin:deploy',
		'copy:deploy',
        'usebanner'
    ]);

  	grunt.initConfig(config);

	for (name in pkg.devDependencies) {
	    if (name.slice(0, 6) === 'grunt-') {
	    	grunt.loadNpmTasks(name);
	    }
	}

  	_results = [];

  	for (taskName in tasks) {
    	taskArray = tasks[taskName];
    	_results.push(grunt.registerTask(taskName, taskArray));
  	}

  	return _results;
};



