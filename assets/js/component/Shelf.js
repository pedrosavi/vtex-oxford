APP.component.Shelf = ClassAvanti.extend({
    init: function (settings) {
        this.settings = settings;
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.settings = $.extend({
            scope: '',
            content: '',
            bindHover: true,
            getSkuInit: false,
            isMulti: false
        }, this.settings);

        this.scope = this.settings.scope;
        this.content = this.settings.content;
        this.bindHover = this.settings.bindHover;
        this.getSkuInit = this.settings.getSkuInit;
        this.getMulti = this.settings.isMulti;

        this.shelfItem = $('.shelf__product-item');
    },

    start: function () {
        if (this.content != '') {
            this.addContent();
        }

        // this.customPrice();
    },

    addContent: function () {
        this.scope.html(this.content);
    },

    customPrice: function () {
        var valor      = this.scope.find('.product-item__list-price').text().replace('R$ ', '');
        var valorMenor = valor.split(',')[0];
        var valorMaior = valor.split(',')[1];

        this.scope.find('.product-item__list-price').html('<span class="rs">R$</span><span class="int-value">' + valorMenor + '</span><span class="decimal-value">,' + valorMaior + '</span>');
    },

    bind: function () {
        var _this = this;
    }
});