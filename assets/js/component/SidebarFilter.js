APP.component.SidebarFilter = ClassAvanti.extend({
    init: function (settings) {
        this.settings = settings;
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.settings = $.extend({
            cleanSingleFilterName: true,
            cleanMultipleFilterName: true,
            createBrandsList: false,
            closeAllFiltersAndOpenFirst: true
        }, this.settings);

        this.singleMenuAll = $('.search-single-navigator');
        this.singleMenu = $('.search-single-navigator').eq(0);
        this.multipleMenu = $('.search-multiple-navigator').eq(0);
        this.submenu = this.singleMenu.find('h4 + ul');
        this.submenuContainer = $('<div class="sidebar__category_submenu"></div>');
        this.submenuContainerBrands = $('<div class="sidebar__brands_submenu"></div>');
        this.categoryName = this.multipleMenu.find('h3 a');
        this.multipleFilters = this.multipleMenu.find('label');
        this.sidebar = $('.sidebar');
        this.brandsList = this.singleMenu.find('ul.Marca');
    },

    start: function () {
        this.hideSingleMenu();

        if (this.settings.cleanSingleFilterName) {
            this.cleanSingleFilterName();
        }

        if (this.settings.cleanMultipleFilterName) {
            this.cleanMultipleFilterName();
        }

        if (this.settings.createBrandsList) {
            this.createBrandsList();
        }

        this.createSidebarMenuSubCategory();

        if (this.settings.closeAllFiltersAndOpenFirst) {
            this.closeAllFiltersAndOpenFirst();
        } else {
            this.OpenAllFilters();
        }

        this.showMultipeMenu();
    },

    hideSingleMenu: function () {
        var _this = this;

        if (_this.multipleMenu.length) {
            _this.singleMenu.hide();
        } else {
            _this.singleMenu.find('ul').each(function() {
                if (!$(this).html().length) {
                    $(this).prev('h3').hide();
                    $(this).hide();
                }
            });
        }
    },

    cleanSingleFilterName: function () {
        var exp = /\([0-9]+?\)/g;

        this.singleMenuAll.find('a').each(function () {
            var self = $(this),
                text = self.text()

            self.text(text.replace(exp, ''))
        });
    },

    cleanMultipleFilterName: function () {
        var exp = /\([0-9]+?\)/g;

        this.multipleFilters.each(function () {
            var self = jQuery(this),
                text = self.text()
                input = self.find('input').clone();

            self.text(text.replace(exp, ''))
            self.prepend(input)
        });
    },

    showMultipeMenu: function () {
        this.multipleMenu.show();
    },

    createSidebarMenuSubCategory: function() {
        var _this = this;

        if (_this.submenu.length) {

            _this.multipleMenu.find('h3').after(_this.submenuContainer);
            _this.submenuContainer.append('<fieldset class="refino even"><h4>' + _this.categoryName.text() + '</h4><div><ul></ul></div></fieldset>');

            _this.submenu.each(function(e) {
                var titleSubmenu = $(this).prev('h4').find('a')[0].outerHTML;
                _this.submenuContainer.find('div ul').append('<li>' + titleSubmenu + '</li>');
            });
        }
    },

    closeAllFiltersAndOpenFirst: function () {
        $('.sidebar').find('fieldset').each(function (elm) {
            var self = jQuery(this),
                div = self.find('> div');

            if (elm < 1) {
                self.addClass('open');
            } else {
                div.hide();
            }
        })
    },

    OpenAllFilters: function () {
        $('.sidebar').find('fieldset').each(function (elm) {
            var self = jQuery(this);

            self.addClass('open');
        })
    },

    openFilterBox: function (target) {
        var div = target.find('> div');

        target.addClass('open');
        div.slideDown();
    },

    closeFilterBox: function (target) {
        var div = target.find('> div');

        target.removeClass('open');
        div.slideUp();
    },

    openFilterBoxMobile: function (target) {
        target.prev().addClass('open');
        target.slideDown();
    },

    closeFilterBoxMobile: function (target) {
        target.prev().removeClass('open');
        target.slideUp();
    },

    createBrandsList: function() {
        var _this = this;

        if (_this.brandsList.length) {
            _this.multipleMenu.find('h3').after(_this.submenuContainerBrands);
            _this.submenuContainerBrands.append('<fieldset class="refino even"><h4>Marcas</h4><div>' + _this.brandsList[0].outerHTML + '</div></fieldset>');
        }
    },

    bind: function () {
        var _this = this;

        _this.sidebar.on('click', 'fieldset h4', function() {
            var self = jQuery(this),
                parent = self.parent();

            if (parent.hasClass('open')){
                _this.closeFilterBox(parent);
            } else {
                _this.openFilterBox(parent);
            }
        });

        _this.sidebar.on('click', 'h5', function() {
            var self = jQuery(this),
                next = self.next('ul');

            if (self.hasClass('open')) {
                _this.closeFilterBoxMobile(next);
            } else {
                _this.openFilterBoxMobile(next);
            }
        });
    }
});