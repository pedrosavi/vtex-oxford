var AvantiSearch = new function () {
    // Define as variáveis do componente
    this.url = null,
    this.result = null,
    this.resultPerPage = null,
    this.currentPage = 1,
    this.currentFilterOrder = '',
    this.currentFilterSpec = [],
    this.settings = {
        resultTarget: jQuery('.avantiSearch-target'),
        btnOrder: jQuery('.filter-order li a'),
        selectOrder: jQuery('.avantiSearch-order-by'),
        listOrder: jQuery('.filter-order ul li'),
        btnSpec: jQuery('.filter-spec li a'),
        btnLoadMore: jQuery('.avantiSearch-load-more'),
        btnFilter: jQuery('.avantiSearch-btn-filter'),
        btnCleanAllFilters: jQuery('.avantiSearch-btn-clean-all-filters'),
        scrollTop: 0,
        loading: jQuery('.avantiSearch-loading'),
        orderBy: jQuery('.order-by'),
        gridClass: ''
    };

    this.init = function (object) {
        this.config(object);
        this.start();
        this.bind();
    },

    // Método que troca as configurações defaults
    this.config = function (object) {
        for (var attrname in object) {
            this.settings[attrname] = object[attrname];
        }

        this.resultTarget = this.settings.resultTarget,
        this.btnOrder = this.settings.btnOrder,
        this.selectOrder = this.settings.selectOrder,
        this.listOrder = this.settings.listOrder,
        this.btnSpec = this.settings.btnSpec,
        this.btnLoadMore = this.settings.btnLoadMore;
        this.btnFilter = this.settings.btnFilter;
        this.btnCleanAllFilters = this.settings.btnCleanAllFilters;
        this.scrollTop = this.settings.scrollTop;
        this.loading = this.settings.loading;
        this.orderBy = this.settings.orderBy;
        this.gridClass = this.settings.gridClass;

        return this;
    };

    // Define a url e o resultPerPage, e executa a busca inicial
    this.start = function () {
        this.url = this.getSearchUrl();

        if (this.url != '') {
            this.resultPerPage = this.getUrlVars(this.url)['PS'];
            this.showLoading();
            this.hideLoadMore();
            this.search();

            if (this.selectOrder.length > 0) {
                this.cleanSelectOrder();
            }
        } else {
            this.resultTarget.append('<span class="search--no-results">Nenhum resultado encontrado</span>');
            this.hideLoading();
            this.hideOrderBy();
        }

        this.checkBtnSpec();
    };

    // Limpa o onclick inline do select order
    this.cleanSelectOrder = function () {
        this.selectOrder.removeAttr('onchange');
    };

    // Metodo copiado do vtex-smartResearch.dev.js
    // Buscar a url interna da busca
    this.getSearchUrl = function () {
        var _this = this,
            url,
            content,
            preg;

        jQuery("script:not([src])").each(function(){
            content=jQuery(this)[0].innerHTML;
            preg=/\/buscapagina\?.+&PageNumber=/i;

            if(content.search(/\/buscapagina\?/i)>-1) {
                url=preg.exec(content);
                return false;
            }
        })

        if(typeof(url)!=="undefined" && typeof(url[0])!=="undefined") {
            return url[0];
        } else {
            //console.log("Não foi possível localizar a url de busca da página. [Método: getSearchUrl]");
            return "";
        }

        return url;
    };

    // O método busca pelas variaveis da url de busca, e retorna seus valores
    // Usado para pegar o número de resultados por página
    this.getUrlVars = function (url) {
        var vars = {};
        var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    };

    // Adiciona um filtro de ordenação. Os valores podem ser: &O=OrderByPriceASC, &O=OrderByPriceDESC, &O=OrderByTopSaleDESC, &O=OrderByReviewRateDESC, &O=OrderByNameASC, &O=OrderByNameDESC, &O=OrderByReleaseDateDESC
    // Se o valor passado for vazio, ele reseta o filtro de ordenação
    this.setFilterOrder = function (value) {
        this.currentFilterOrder = value != '' ? value : '';
    };

    // Adiciona um filtro de especificação, o valor de ser um array contendo o filtro e o nome
    // Exemplo ['&fq=B:355', 'Marca 1']
    this.addFilterSpec = function (arr) {
        this.currentFilterSpec.push(arr);
    };

    // Remove um filtro de especificação, o valor de ser um array contendo o filtro e o nome
    // Exemplo ['&fq=B:355', 'Marca 1']
    this.removeFilterSpec = function (arr) {
        this.currentFilterSpec = this.removeFilterSpecArray(this.currentFilterSpec, arr);
    };

    // Busca e exclui o array dentro do array currentFilterSpec
    // Esse método é chamado pelo removeFilterSpec
    this.removeFilterSpecArray = function (arr, arr2) {
        var newArray = [],
            length = arr.length;

        for (i=0; i < length; i++) {
            if (arr[i][0] != arr2) {
                newArray.push([arr[i][0], arr[i][1]]);
            }
        }

        return newArray;
    };

    // Remove todos os filtros de especificação
    this.resetFilterSpec = function () {
        this.currentFilterSpec = [];
    };

    // Trata o array currentFilterSpec, coloca todas as especificações em sequencia e retorna uma string
    // Esse método é chamado pelo search
    this.getFilterSpec = function () {
        var _this = this,
            string = '',
            length = _this.currentFilterSpec.length;

        for (i=0; i < length; i++) {
            string += _this.currentFilterSpec[i][0];
        }

        return string;
    };

    // Remove todos os resultados ja listados e zera as variaveis currentPage e result
    // Se for passado uma função de callback, ele executa a mesma
    this.resetResult = function (callback) {
        var _this = this;

        this.showLoading();
        this.hideLoadMore();

        this.resultTarget.stop(true, true).slideUp(1000, function () {
            ul = _this.resultTarget.find('ul').remove();
            lines = _this.resultTarget.find('.avantiSearch-separador').remove();

            _this.currentPage = 1,
            _this.result = null;

            if (callback) {
                callback();
            }
        })
    };

    // Adiciona +1 ao currentPage e realiza a busca na próxima página
    this.loadNext = function () {
        this.showLoading();
        this.hideLoadMore();
        this.currentPage++;
        this.search();
    };

    // Busca via ajax os resultados da url de busca mais os filtros selecionados
    // Ele monsta da seguinte forma: url de busca + pagina atual da paginação + filtros de especificação + filtro de ordenação
    // Para funcionar ele precisa ter no mínimo url de busca + pagina atual da paginação, os filtros podem ou não ser passados
    // Após sucesso do ajax, ele define a variavel result e  chama o método checkResult
    this.search = function () {
        var _this = this;

        jQuery.ajax({
            url: 'http://' + document.domain + _this.url + _this.currentPage + _this.getFilterSpec() + _this.currentFilterOrder,
            type: 'GET',
            beforeSend: function () {
                //console.log('carregando resultado');
            },
            success: function (data) {
                //console.log('resultado carregado');
                _this.hideLoading();
                _this.resultTarget.stop(true, true).show();
                _this.result = jQuery(data);
                _this.checkResult();
            },
            error: function () {
                _this.resultTarget.append('<span class="search--no-results">Nenhum resultado encontrado</span>');
                _this.hideLoading();
                _this.hideOrderBy();
                //console.log('Erro ao receber resultado de busca');
            }
        });
    };

    // Trata a variável result, setada pelo método search
    // Cria um <ul> e insere os <li> recebidos, desconsiderando os helperComplement da vtex
    this.checkResult = function () {
        var _this = this,
            itens = this.result.find('ul:eq(0) > li').not('.helperComplement');

        if (itens.length > 0) {

            if (this.currentPage > 1) {
                var separador = jQuery('<span class="avantiSearch-separador"></span>').appendTo(_this.resultTarget);
                separador.hide();
            }

            var newUl = jQuery('<ul></ul>').appendTo(_this.resultTarget);


            newUl.stop(true, true).slideUp(1000);

            itens.each(function () {
                var item = jQuery('<li></li>');

                new APP.component.Shelf({
                    scope: item,
                    truncateLimit: 309,
                    content: '<div class="' + _this.gridClass + '">' + jQuery(this).html() + '</div>'
                });

                item.appendTo(newUl);
            });

            if (this.currentPage > 1) {
                separador.show();
            }


            newUl.stop(true, true).slideDown(1000, function () {
                if (itens.length < _this.resultPerPage) {
                    _this.hideLoadMore();
                } else {
                    _this.showLoadMore();
                }
            });
        }

    };

    this.showLoadMore = function () {
        this.btnLoadMore.stop(true,true).fadeIn();
    };

    this.hideLoadMore = function () {
        this.btnLoadMore.stop(true,true).fadeOut();
    };

    this.showLoading = function () {
        this.loading.stop(true,true).fadeIn();
    };

    this.hideLoading = function () {
        this.loading.stop(true,true).fadeOut();
    };

    this.hideOrderBy = function () {
        this.orderBy.stop(true,true).fadeOut();
    };

    this.checkBtnSpec = function () {
        if (this.btnSpec.length) {
            this.btnFilter.show();
        }
    };

    this.cleanAllFilters = function() {
        var _this = this;

        this.resetResult(function () {
            _this.btnCleanAllFilters.addClass('disabled');

            _this.btnSpec.removeClass('on');
            _this.btnSpec.find('.multi-search-checkbox').attr('checked', false);

            _this.currentFilterSpec = [];
            _this.search();
        });
    },

    this.checkFilterSelected = function() {
        var _this = this;

        if (this.btnSpec.find('.multi-search-checkbox:checked').length) {
            _this.btnCleanAllFilters.removeClass('disabled');
        } else {
            _this.btnCleanAllFilters.addClass('disabled');
        }
    },

    // Bind dos elementos
    this.bind = function () {
        var _this = this;

        this.selectOrder.on({
            change: function () {
                var self = jQuery(this),
                    value = self.val(),
                    order = (value != '') ? '&O=' + value : value;

                _this.setFilterOrder(order);
                _this.resetResult(function () {
                    _this.search();
                });
            }
        })

        this.listOrder.on({
            click: function () {
                var self = jQuery(this),
                    value = self.data('value'),
                    order = '&O=' + value;


                if (self.hasClass('on')) {
                    self.removeClass('on');
                    order = '';
                } else {
                    _this.listOrder.removeClass('on');
                    self.addClass('on');
                }

                _this.setFilterOrder(order);
                _this.resetResult(function () {
                    _this.search();
                });
            }
        })

        this.btnSpec.on({
            click: function (event) {
                if (event.target.nodeName == 'LABEL') return;

                var self = jQuery(this),
                    text = self.text(),
                    rel = self.find('input').attr('rel');

                if (self.hasClass('on')){
                    self.removeClass('on');
                    _this.removeFilterSpec(['&'+ rel]);
                    if (!_this.btnFilter.length) {
                        _this.resetResult(function () {
                            _this.search();
                        });
                    }
                } else {
                    self.addClass('on');
                    _this.addFilterSpec(['&'+ rel, text]);
                    if (!_this.btnFilter.length) {
                        _this.resetResult(function () {
                            _this.search();
                        });
                    }
                }

                _this.checkFilterSelected();
            }
        });

        this.btnFilter.on({
            click: function (event) {
                $('html, body').animate({
                    scrollTop: _this.scrollTop
                });

                _this.resetResult(function () {
                    _this.search();
                });
            }
        });

        this.btnCleanAllFilters.on('click', function() {
            if (!$(this).hasClass('disabled')) {
                $('html, body').animate({
                    scrollTop: _this.scrollTop
                });

                _this.cleanAllFilters();
            }
        });

        this.btnLoadMore.on({
            click: function () {
                _this.loadNext();
            }
        })
    };

};
