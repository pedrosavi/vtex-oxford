APP.component.Popup = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.body = $('body');
        this.btnOpen = '.modal-open';
        this.btnClose = '.close';
        this.modals = $('.modal');
    },

    start: function () {
    },

    openPopup: function(target){
        $(target).fadeIn();
        this.body.addClass('modal-open');
    },

    closePopup: function(){
        this.modals.fadeOut();
        this.body.removeClass('modal-open');
    },

    bind: function () {
        var _this = this;

        _this.body.on('click', _this.btnOpen, function(e) {
            e.preventDefault();
            _this.openPopup('#' + $(this).data('target'));
        });

        _this.body.on('click', _this.btnClose, function(e) {
            _this.closePopup();
        });

        _this.body.on('click', _this.modals, function(e) {
            if ($('.modal').is(e.target) // if the target of the click isn't the container...
                && $('.modal').has(e.target).length === 0) // ... nor a descendant of the container
            {
                _this.closePopup();
            }
        });
    }
});