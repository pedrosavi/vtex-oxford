APP.component.CollectionCarousel = ClassAvanti.extend({
    init: function (settings) {
        this.settings = settings;
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.settings = $.extend({
            scope: '',
            gridClass: '',
            slickSettings: {
                autoplay: false,
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: "unslick"
                    }
                ]
            }
        }, this.settings);

        this.scope = this.settings.scope;
        this.gridClass = this.settings.gridClass;
        this.slickSettings = this.settings.slickSettings;
    },

    start: function () {
        this.eachCollections();
    },

    eachCollections: function () {
        var _this = this;

        _this.scope.each(function() {
            var itens = $(this).find('ul:eq(0) > li').not('.helperComplement');
            $(this).find('div:eq(0)').remove()
            _this.checkResult($(this) , itens);
        });
    },

    checkResult:function(shelf, itens) {
        var _this = this;

        if (itens.length > 0) {

            var newUl = jQuery('<ul></ul>').prependTo(shelf);

            itens.each(function () {

                var item = jQuery('<li></li>');

                new APP.component.Shelf({
                    scope: item,
                    content: '<div class="' + _this.gridClass + '">' + jQuery(this).html() + '</div>'
                });

                item.appendTo(newUl);
            });

            _this.carousel(newUl);

            newUl.stop(true, true).show();
        }
    },

    carousel: function(shelf) {
        var _this = this;

        shelf.slick(_this.slickSettings);
    },

    bind: function () {
        var _this = this;

        $(window).on('resize', function() {
            _this.scope.find(' > ul').slick('resize');
        });

        $(window).on('orientationchange', function() {
            _this.scope.find(' > ul').slick('resize');
        });
    }
});