var EntityHelper = new function () {

   this.search = function (vtexid, prefix, querystring, callback) {
        $.ajax({
            url: '//api.vtexcrm.com.br/' + vtexid + '/dataentities/' + prefix + '/search?' + querystring + '',
            type: 'GET',
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json',
                'REST-Range': 'resources=0-99'
            },
            success: function (data) {
                console.log(data);

                if(callback){
                    callback(data);
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    };

    this.getItem = function (vtexid, id, prefix, querystring, callback) {
        $.ajax({
            url: '//api.vtexcrm.com.br/' + vtexid + '/dataentities/'+ prefix +'/documents/' + id + '?'+querystring,
            type: 'GET',
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json'
            },
            success: function (data) {

                if(callback){
                    callback(data);
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    };

    this.newItem = function (vtexid, prefix, obj, callback) {
        $.ajax({
            url: '//api.vtexcrm.com.br/' + vtexid + '/dataentities/' + prefix + '/documents',
            type: 'POST',
            data: JSON.stringify(obj),
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json'
            },
            success: function (data) {
                console.log(data);
                if(callback){
                    callback(data);
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    };

    this.updateItem = function (vtexid, id, prefix, obj, callback) {
        console.log(obj);
        $.ajax({
            url: '//api.vtexcrm.com.br/' + vtexid + '/dataentities/' + prefix + '/documents/' + id,
            type: 'PATCH',
            data: JSON.stringify(obj),
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json'
            },
            success: function (data) {
                console.log(data);
                if(callback){
                    callback(data);
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    };

    this.deleteItem = function (vtexid, id, prefix, callback) {
        $.ajax({
            url: '//api.vtexcrm.com.br/' + vtexid + '/dataentities/' + prefix + '/documents/' + id,
            type: 'DELETE',
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json',
                'x-vtex-api-appKey': 'vtexappkey-avanti-UTMYRD',
                'x-vtex-api-appToken': 'WIEACHUBOTTKPEGNXLHMWLZMPXXRWDRMNYAQBTSFGSVUYZAGZYJDTVXLJRLFJCEKCHWRDPLESBGEQENPWRSYTOSRAPHJINVDNKRRCICJMFKRPSUEEVIRKFROOEROIBVF'
            },
            success: function (data) {
                if(callback){
                    callback(data);
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    };
};