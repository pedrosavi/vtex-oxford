APP.component.MenuMobile = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.menuLink = $('.menu--mobile .menu__item--dropdown a');
    },

    start: function () {

    },

    slidetoggleDrop: function (target) {
        target.slideToggle();
    },

    setOpenLink: function (target) {
        if (target.hasClass('open')) {
            target.removeClass('open');
        } else {
            target.addClass('open');
        }
    },

    bind: function () {
        var _this = this;

        this.menuLink.on({
            click: function (event) {
                var parent = $(this).parent(),
                    target = parent.find(' > .submenu');

                if (target.length) {
                    event.preventDefault();
                    _this.slidetoggleDrop(target);
                    _this.setOpenLink(parent);
                }
            },
        })
    }
});