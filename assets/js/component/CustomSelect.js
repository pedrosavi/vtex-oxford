APP.component.CustomSelect = ClassAvanti.extend({
    init: function(settings) {
        this.settings = settings;
        this.setup();
        this.start();
        this.bind();
    },

    setup: function() {
        this.settings = jQuery.extend({
            scope: '',
            span: '',
            select: '',
            openLink: false,
            callback: function (value, text) {}
        }, this.settings);

        this.scope = this.settings.scope,
        this.span = this.settings.span,
        this.select = this.settings.select;
        this.callback = this.settings.callback;
        this.openLink = this.settings.openLink;
    },

    start: function () {
        var text = this.select.find('option:selected').text();
        this.setText(text);
    },

    setText: function (text) {
        this.span.text(text);
    },

    change: function () {
        var text = this.select.find('option:selected').text(),
            value = this.select.find('option:selected').val();

        if (this.openLink && value != '') {
            document.location = value;
        }

        this.setText(text);
        this.callback(value, text);
    },

    bind: function () {
        var _this = this;

        this.select.on({
            change: function () {
                _this.change();
            }
        });
    }
});