APP.component.Newsletter = ClassAvanti.extend({
    init: function (settings) {
        this.settings = settings;
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.settings = $.extend({
            scope: '',
            vtexid: '',
            prefix: 'NW',
            errorMessageType: 'text', // alert, text
            customFields: function (jsonForm) {
                return jsonForm;
            },
            success: function() {
                this.successMessage();
            }
        }, this.settings);

        this.scope = this.settings.scope;
        this.vtexid = this.settings.vtexid;
        this.prefix = this.settings.prefix;
        this.errorMessageType = this.settings.errorMessageType;
        this.customFields = this.settings.customFields;
        this.success = this.settings.success;

        this.name = this.scope.find('.newsletter__nome');
        this.email = this.scope.find('.newsletter__email');
        this.submit = this.scope.find('.newsletter__submit');
        this.jsonForm = {};
    },

    start: function () {

    },

    sendNewsletter: function () {
        var _this = this;

        _this.errorClean();

        if(_this.email.val() == '' || !_this.validateEmail(_this.email.val())) {

            switch (this.errorMessageType) {
                case 'alert':
                    _this.errorAlert();
                    break;
                case 'text':
                    _this.errorText();
                    break;
            }

            _this.scope.addClass('newsletter--error');
        } else {
            _this.submit.addClass('newsletter__submit--loading');

            _this.jsonForm['nome'] = _this.name.val();
            _this.jsonForm['email'] = _this.email.val();
            _this.jsonForm = _this.customFields(_this.jsonForm);

            EntityHelper.newItem(_this.vtexid, _this.prefix, _this.jsonForm, function(data) {
                _this.success();
            });
        }
    },

    errorAlert: function() {
        alert('Email inválido.');
    },

    errorText: function() {
        var _this = this;

        _this.email.closest('.form-group').addClass('form-group--error');
        _this.email.after('<span class="newsletter__error text-danger">E-mail inválido</span>')
    },

    errorClean: function() {
        this.scope.removeClass('newsletter--error');
        this.scope.find('.newsletter__error').remove();
        this.email.closest('.form-group').removeClass('form-group--error');
    },

    successMessage: function() {
        var _this = this;

        _this.scope.fadeOut('600', function() {
            _this.scope.after('<div class="newsletter__form--success"><span class="text-success">Cadastro efetuado com sucesso!</span></div>');
        });
    },

    validateEmail: function(email) {
        var regEx = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regEx.test(email);
    },

    bind: function () {
        var _this = this;

        _this.submit.click(function(e){
            e.preventDefault();
            _this.sendNewsletter();
        });
    }
});