APP.core.Main = ClassAvanti.extend({
    init: function() {
        this.start();
    },

    start: function () {
        APP.i.util = new APP.core.Util();
        APP.i.general = new APP.controller.General();
        this.loadPageController();
    },

    loadPageController: function () {
        var controller = APP.i.util.getController();

        if(controller) {
            APP.i.currentController = new APP.controller[controller]();
        }
    }
});