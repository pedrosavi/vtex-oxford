var APP = {
    core: {},
    component: {},
    controller: {},
    i: {}
};

$(window).load(function() {
    new APP.core.Main();
});

$(window).ready(function() {
    var zoomWidth = 420;
    var zoomHeight = 420;

    LoadZoom = function(pi) {
        if ($(".image-zoom").length > 0) {
            var optionsZoom = {
                preloadText: "",
                title: false,
                zoomWidth: zoomWidth,
                zoomHeight: zoomHeight,
            };
            $(".image-zoom").jqzoom(optionsZoom);
        }
    }
});
