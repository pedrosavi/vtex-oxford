APP.core.Util = ClassAvanti.extend({
    getController: function () {
        var controller = jQuery('meta[name=controller]').attr('content');
        return controller ? controller : false;
    },

    screenW: function () {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    },

    screenH: function () {
        return $(window).height();
    },

    scrollTop: function () {
        return $('body').scrollTop();
    },

    scrollTopFF: function () {
        return $('html').scrollTop();
    },

    getUrl: function () {
        return document.URL;
    },

    verifyLogin: function () {
        var login = (document.cookie.indexOf("VtexIdclientAutCookie_oxfordporcelanas") >= 0) ? true : false;
        return login;
    },

    setDecimal: function(value, string){
        var dotValue = value.substr(0, value.length - 2) + string + value.substr(value.length - 2, value.length);
        return dotValue;
    },

    capitalizeFirstLetter: function(string) {
        string = string.toLowerCase();
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    clearRedirect: function (url) {
        var urlReplaced = url.replace("redirect=true", "redirect=false");

        return urlReplaced;
    }
});