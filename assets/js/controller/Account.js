APP.controller.Account = ClassAvanti.extend({
	init: function () {
		this.setup();
		this.start();
		this.bind();
	},

	setup: function () {
		this.body = $('body');
		APP.i.popup = new APP.component.Popup();
	},

	start: function () {
		this.changeDOM();
	},

	changeDOM: function() {
		this.changeModalDOM();

		$('.profile-detail-display').addClass('col-sm-6').wrapInner('<div class="well"></div>');
		$('.address-display-block').addClass('col-sm-6').wrapInner('<div class="well"></div>');

		this.body.addClass('loaded');
	},

	changeModalDOM: function() {
		$('.account__main a[data-toggle="modal"]').each(function() {
            var href = $(this).attr('href');

            if (href) {
                var target = href.replace('#', '');

                $(this).attr('data-target', target);
                $(this).addClass('modal-open');
            }
        });

		$('.modal').each(function() {
			$(this).removeClass('hide');
			$(this).addClass('in');
			$(this).wrapInner('<div class="modal-dialog"><div class="modal-content"></div></div>');

			$(this).find('.close').addClass('modal-close');
		});

		$('.account-content').addClass('row');
	},

	businessToggleTxt: function() {
		if ($('#business-toggle').attr('data') == 'off') {
        	$('#business-toggle').text('Incluir dados de pessoa jurídica');
		} else {
        	$('#business-toggle').text('Não usar dados de pessoa jurídica');
		}
	},

	bind: function () {
		var _this = this;

		$('#edit-data-link, #business-toggle').on('click', function() {
			_this.businessToggleTxt()
		});
	}
});