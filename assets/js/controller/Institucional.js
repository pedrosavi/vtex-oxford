APP.controller.Institucional = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.sidebar      = $('.institucional__sidebar');
        this.documentPath = document.location.pathname;
        this.customSelect = $('.fake-select');
        this.sumaryMenuLink = $('ul li a');
    },

    start: function () {
        this.addClassCurrentItem();
        this.startCustomSelect();
    },

    addClassCurrentItem: function() {
        var _this = this;

        _this.sidebar.find('a[data-href="' + _this.documentPath + '"]').addClass('current');
        _this.sidebar.find('select').val(_this.documentPath);
    },

    startCustomSelect: function() {
        APP.i.customSidebar = new APP.component.CustomSelect({
            scope: this.customSelect,
            span: this.customSelect.find('span'),
            select: this.customSelect.find('select'),
            openLink: true
        });
    },

    bind: function () {
        var _this = this;

        _this.sumaryMenuLink.on('click', function(e) {
            e.preventDefault();
            var target = $(this).attr('href');
            var topDistance  = $(target).offset().top - 90;

            $('html, body').animate({scrollTop:topDistance}, 'slow');
        })
    }
});