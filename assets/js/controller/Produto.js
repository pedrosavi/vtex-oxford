APP.controller.Produto = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.thumbs = $('.thumbs');
        this.relatedProducts = $('.product-main__related-products');
        this.collectionQVVT = $('.collection--qvvt');
        this.buyTogether = $('.product__buy-together');
        this.skus = $('.product-main__skus');
        this.buyButton = $(".buy-button");
        this.characteristics = $('#caracteristicas');
        this.quantity = $(".product-main__quantity");
        this.quantityPlus = $(".product-main__quantity__plus");
        this.quantityMinus = $(".product-main__quantity__minus");
        this.infoVideoTab = $('.tab-pane--video');
        this.infoVideoPanel = $('.product-main__info__video--body');
        this.infoPiecesTab = $('.tab-pane--pieces');
        this.infoPiecesPanel = $('.product-main__info__pieces--body');
    },

    start: function () {
        this.startCarouselThumbs();
        this.addDataNumThumb();
        this.setCurrentThumb(0);
        this.checkRelatedProducts();
        this.notifyme();
        this.startInfo();
        this.changeQuantity();
        this.quantityAdd();
        this.quantityRemove();
        this.checkQVVT();
        this.checkBuyTogether();
        this.skuCustomSelect();
        this.checkSingleSku()

        // Função da vtex para abrir o o box de frete
        ShippingValue();
    },

    startCarouselThumbs: function() {
        var _this = this;

        _this.thumbs.slick({
            vertical: true,
            autoplay: false,
            dots: false,
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 5,
            arrows: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        vertical: false,
                        autoplay: false,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: true,
                        infinite: false,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        vertical: false,
                        autoplay: false,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false,
                        infinite: true,
                        centerMode: true,
                    },
                }
            ]
        });
    },

    setCurrentThumb: function(currentThumb) {
        if (currentThumb == 0) {
            var currentThumb = $('.slick-current').data('num');
        }
        this.thumbs.find('li').removeClass('thumbs__item--current');
        this.thumbs.find('li[data-num="' + currentThumb + '"]').addClass('thumbs__item--current');
    },

    addDataNumThumb:function() {
        var _this = this;
        var count = 1;

        _this.thumbs.find('li').each(function() {
            $(this).attr('data-num', count);
            count++;
        });
    },

    checkRelatedProducts: function() {
        var _this = this;

        if (_this.relatedProducts.find('ul').length) {
            _this.relatedProducts.slideDown();
        }
    },

    checkQVVT: function() {
        var _this = this;

        if (_this.collectionQVVT.find('ul').length) {
            _this.collectionQVVT.removeClass('hide');
            _this.collectionQVVT.find('ul.slick-slider').slick('resize');
        }
    },

    checkBuyTogether: function() {
        var _this = this;

        setTimeout(function() {
            if (_this.buyTogether.find('table').length) {
                _this.buyTogether.removeClass('hide');

                _this.buyTogether.find('table').find('.buy').contents().filter(function() {
                    if (this.nodeType == Node.TEXT_NODE && this.nodeValue.indexOf('Valor total') != -1) {
                        var txt = $(this).text();
                        txt = txt.replace(' Valor total:  ', '');
                        _this.buyTogether.find('table').find('.buy').prepend('<strong class="product__buy-together__total">' + txt + '</strong>');
                        this.remove();
                    }
                });
            } else {
                _this.buyTogether.addClass('hide');
            }
        }, 1000);
    },

    notifyme: function() {
        $('.notifyme-client-email').on('keyup', function() {
            var email = $(this).val();
            $('.notifyme-client-name').val(email);
        });
    },

    startInfo: function() {
        var _this = this;

        var infoVideo = _this.characteristics.find('.value-field.Video');
        if (infoVideo.length) {
            var infoVideoIframe = infoVideo.clone();
            _this.infoVideoTab.html(infoVideoIframe.html());
            _this.infoVideoPanel.html(infoVideoIframe.html());

            _this.infoVideoTab.closest('.product-main__info').find('.nav-tabs__item--video').removeClass('hide');
            _this.infoVideoPanel.closest('.panel-default').removeClass('hide');
        }
    },

    changeQuantity: function() {
        var _this = this;
        this.quantity.find(".product-main__quantity__value").on("change", function(e) {
            _this.quantityValue()
        })
    },

    quantityValue: function() {
        var _this = this;
        var qty = _this.quantity.find(".product-main__quantity__value").val();
        var href = _this.buyButton.attr("href");
        if (parseInt(qty) <= 0) {
            qty = 1;
            _this.quantity.find(".product-main__quantity__value").val(qty)
        }
        href = href.replace(/qty=[0-9]{1,10}/g, "qty=" + qty);
        _this.buyButton.attr("href", href)
    },

    quantityAdd: function() {
        var _this = this;
        _this.quantityPlus.on("click", function() {
            var qty = _this.quantity.find(".product-main__quantity__value").val();
            qty = parseInt(qty) + 1;
            _this.quantity.find(".product-main__quantity__value").val(qty);
            _this.quantityValue()
        })
    },

    quantityRemove: function() {
        var _this = this;
        _this.quantityMinus.on("click", function() {
            var qty = _this.quantity.find(".product-main__quantity__value").val();
            qty = parseInt(qty) - 1;
            _this.quantity.find(".product-main__quantity__value").val(qty);
            _this.quantityValue()
        })
    },

    checkCustomFields: function(skuId) {
        var _this = this;

        EntityHelper.getItem('oxfordporcelanas', skuId, 'QP', '_fields=qtd_pecas', function(data) {
            if (typeof data.qtd_pecas != 'undefined') {
                _this.infoPiecesTab.html(data.qtd_pecas);
                _this.infoPiecesPanel.html(data.qtd_pecas);
                _this.infoPiecesTab.closest('.product-main__info').find('.nav-tabs__item--pieces').removeClass('hide');
                _this.infoPiecesPanel.closest('.panel-default').removeClass('hide');
            } else {
                _this.infoPiecesTab.html('');
                _this.infoPiecesPanel.html('');
                _this.infoPiecesTab.closest('.product-main__info').find('.nav-tabs__item--pieces').addClass('hide');
                _this.infoPiecesPanel.closest('.panel-default').addClass('hide');
            }
        });
    },

    checkSingleSku: function() {
        if (vtxctx.skus.indexOf(';') == -1) {
            this.checkCustomFields(vtxctx.skus);
        }
    },

    skuCustomSelect: function() {
        var _this = this;

        _this.skus.find('select').each(function() {
            $(this).wrap('<div class="fake-select"></div>');

            var val = $(this).val();
            if ($(this).val() == '') {
                var val = 'Selecione';
                $(this).find('option').eq(0).text(val);
            }

            $(this).before('<span>' + val + '</span>');

            var skuSelect = $(this).closest('.fake-select');

            APP.i.customSku = new APP.component.CustomSelect({
                scope: skuSelect,
                span: skuSelect.find('span'),
                select: skuSelect.find('select')
            });
        });
    },

    AvantiOnSkuSelectionChanged: function(e) {
        APP.i.currentController.checkCustomFields(e.newSkuId);
    },

    bind: function () {
        var _this = this;

        // Adiciona listener no evento skuSelectionChanged
        var AvantiskuDataFetcherListener = new Vtex.JSEvents.Listener('skuDataFetcher', _this.AvantiOnSkuSelectionChanged);
        skuEventDispatcher.addListener(skuSelectionChangedEventName, AvantiskuDataFetcherListener);

        _this.thumbs.on('click', 'li', function() {
            var currentThumb = $(this).data('num');
            _this.setCurrentThumb(currentThumb);
        });

        _this.skus.find('select').on('change', function() {
            _this.thumbs.hide();
            _this.thumbs.slick('destroy');

            DelayedActions.add({
                condition: function () {
                    return ($('.thumbs li').length > 0);
                },
                callback: function () {
                    _this.thumbs = $('.thumbs');
                    _this.startCarouselThumbs();
                    _this.addDataNumThumb();
                    _this.setCurrentThumb(0);
                    _this.thumbs.show();
                    _this.thumbs.slick('resize');
                    _this.checkBuyTogether();

                    _this.skus.find('select').each(function() {
                        if ($(this).val() == '') {
                            $(this).closest('.fake-select').find('span').text('Selecione');
                        }
                    });
                },
                persistent: false
            });
        });
    }
});