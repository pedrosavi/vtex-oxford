APP.controller.General = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.newsletter = $('.newsletter__form');
        this.bannerSubmenu = $('.submenu-banner');
        this.header = $('.header');
        this.headerFixed = $('.header--fixed');
        this.navbarDesktop = $('.navbar--desktop');
        this.welcome = $('.header__welcome');
        this.collectionsCarousel = $('.collection--carousel .shelf');
        this.minicart = $('.minicart-count');
    },

    start: function () {
        APP.i.dropContent = new APP.component.DropContent();
        APP.i.menuMobile = new APP.component.MenuMobile();

        this.startNewsletter();
        this.setLinkBtnSubmenuBanner();
        this.checkUserAuth();
        this.startCollectionCarousel();
        this.refreshMinicart();
    },

    setLinkBtnSubmenuBanner: function() {
        var _this = this;

        _this.bannerSubmenu.each(function() {
            var link = $(this).find('.submenu-banner__img a').attr('href');

            if (link != '') {
                $(this).find('.btn').attr('href', link);
            }
        });
    },

    startCollectionCarousel: function () {
        var _this = this;

        new APP.component.CollectionCarousel({
            scope: _this.collectionsCarousel,
            gridClass: 'col-sm-12',
            slickSettings: {
                autoplay: false,
                dots: true,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: false,
                            centerMode: true,
                            dots: false
                        }
                    }
                ]
            }
        });
    },

    startNewsletter: function() {
        var _this = this;

        new APP.component.Newsletter({
            vtexid: 'oxfordporcelanas',
            scope: _this.newsletter
        });
    },

    mouseOutMenuFixed: function() {
        var  _this = this;

        if (!_this.navbarDesktop.is(':hover') && !_this.headerFixed.find('.navbar-toggle').is(':hover')) {
            _this.headerFixed.find('.navbar-toggle').removeClass('navbar-toggle--hover');
            _this.headerFixed.removeClass('header--fixed--hover');
            _this.navbarDesktop.css('top', '0');
        }
    },

    mouseOverMenuFixed: function() {
        var  _this = this;

        _this.headerFixed.find('.navbar-toggle').addClass('navbar-toggle--hover');
        _this.headerFixed.addClass('header--fixed--hover');
        _this.navbarDesktop.css('top', '75px');
    },

    checkUserAuth: function() {
        var _this = this;

        if (!APP.i.util.verifyLogin()) {
            this.welcome.find('.header__welcome-login').removeClass('hide');
        } else {
            DelayedActions.add({
                condition: function () {
                    return (_this.welcome.find('.welcome').find('em').length > 0);
                },
                callback: function () {
                    var name = _this.getCleanNameAccount();
                    _this.welcome.find('.header__welcome-logout__name').text(name);
                    _this.welcome.find('.header__welcome-logout').removeClass('hide');
                },
                persistent: false
            });
        }
    },

    getCleanNameAccount: function() {
        this.welcome.find('.welcome').find('em').remove();
        var name = this.welcome.find('.welcome').text();
        name = name.replace('.', '');
        name = name.replace('Olá ', '');
        return name;
    },

    refreshMinicart: function() {
        var _this = this;

        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            if (orderForm.items != undefined || orderForm.items != "undefined") {
                var itensCount = 0;
                $.each(orderForm.items, function(index) {
                    itensCount += orderForm.items[index].quantity;
                });

                _this.minicart.html(itensCount);
            }
        });
    },

    bind: function () {
        var _this = this;

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > _this.header.height()) {
                _this.headerFixed.addClass('header--fixed--open');
                _this.navbarDesktop.addClass('navbar--desktop--fixed');
            } else {
                _this.headerFixed.removeClass('header--fixed--open');
                _this.navbarDesktop.removeClass('navbar--desktop--fixed');
            }
        });

        _this.headerFixed.find('.navbar-toggle').on('mouseover', function() {
            _this.mouseOverMenuFixed();
        });

        _this.navbarDesktop.on('mouseover', function() {
            if ($(this).hasClass('navbar--desktop--fixed')) {
                _this.mouseOverMenuFixed();
            }
        });

        _this.headerFixed.find('.navbar-toggle').on('mouseout', function() {
            _this.mouseOutMenuFixed();
        });

        _this.navbarDesktop.on('mouseout', function() {
            _this.mouseOutMenuFixed();
        });
    }
});