APP.controller.Home = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.bannerMainCarousel = $('.banner-main');
    },

    start: function () {
        this.startBannerMainCarousel();
    },

    startBannerMainCarousel: function () {
        var _this = this;

        _this.bannerMainCarousel.slick({
            autoplay: false,
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        });
    },

    bind: function () {

    }
});