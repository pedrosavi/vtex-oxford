APP.controller.Busca = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.body = $('body');
        this.multipleMenu = $('.search-multiple-navigator');
        this.multipleFilters = this.multipleMenu.find('label');
        this.searchTermValue = $('.resultado-busca-termo:eq(0) .value');
        this.pageTitle = $('.search__title');
        this.totalSearchResults = $('.resultado-busca-numero:eq(0) .value').text();
        this.breadcrumb = $('.breadcrumb');
        this.customSelectOrderBy = $('.order-by__fake-select');
    },

    start: function () {
        this.startSidebarFilter();
        this.startAvantiSearch();
        this.searchTerm();
        this.startCustomSelectOrderBy();
    },

    startSidebarFilter: function () {
        var _this = this;

        new APP.component.SidebarFilter({
            closeAllFiltersAndOpenFirst: false,
        });
    },

    startAvantiSearch: function () {
        var _this = this;

        AvantiSearch.init({
            btnSpec: _this.multipleFilters,
            scrollTop: $(".main__header").offset().top - $('.header--fixed').height(),
            gridClass: 'col-sm-6 col-md-4'
        });
    },

    searchTerm: function() {
        var _this = this;

        if (_this.searchTermValue.text() != '') {
            _this.pageTitle.html('<div class="avantiSearch--total"><strong>' + _this.totalSearchResults + ' produtos</strong> encontrados para <strong>' + _this.searchTermValue.text() + '</strong></div>');
            _this.pageTitle.show();
            _this.setSearchBreadcrumb();
        }
    },

    setSearchBreadcrumb: function() {
        if (this.body.hasClass('search') && !this.breadcrumb.find('.busca-texto-livre-elimina').length) {
            this.breadcrumb.find('ul').append('<li><strong><a>Resultado de busca</a></strong></li>');
        }
    },

    startCustomSelectOrderBy: function () {
        APP.i.customOrderBy = new APP.component.CustomSelect({
            scope: this.customSelectOrderBy,
            span: this.customSelectOrderBy.find('span'),
            select: this.customSelectOrderBy.find('select')
        });
    },

    bind: function () {
        var _this = this;
    }
});