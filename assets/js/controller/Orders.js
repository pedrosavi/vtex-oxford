APP.controller.Orders = ClassAvanti.extend({
    init: function () {
        this.setup();
        this.start();
        this.bind();
    },

    setup: function () {
        this.body = $('body');
        this.myOrders = $('.myorders');
    },

    start: function () {
        this.changeDOM();
    },

    changeDOM: function() {
        var _this = this;

        $("LINK[href='//io.vtex.com.br/front-libs/bootstrap/2.3.2/css/bootstrap.min.css']").remove();

        DelayedActions.add({
            condition: function () {
                return ($('.myorders-list .ordergroup').length > 0);
            },
            callback: function () {
                $('.row-fluid').removeClass('row-fluid').addClass('row');

                _this.myOrders.find('.payment-info.span4').removeClass('span4').addClass('col-lg-4');
                _this.myOrders.find('.shipping-info.span4').removeClass('span4').addClass('col-lg-4');
                _this.myOrders.find('.total-info.span4').removeClass('span4').addClass('col-lg-4');
                _this.myOrders.find('.top-row .span3').removeClass('span3').addClass('col-lg-3');
                _this.myOrders.find('.top-row .span9').removeClass('span9').addClass('col-lg-9');
                _this.myOrders.find('.order-detail-items').unwrap('.col-lg-12');
                _this.myOrders.find('.order-detail-items').wrap('<div class="col-lg-12"></div>');
            },
            persistent: false
        });

        _this.body.addClass('loaded');
    },

    bind: function () {

    }
});