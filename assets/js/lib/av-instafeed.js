var avantiInstafeed = new function () {
    this.instagramReturn = null;
    this.objectConfig = {
        instagramUserId: '',
        instagramAccessToken: '',
        quantity: 10,
        successCallback: function (data) {}
    };

    this.config = function (object) {
        for (var attrname in object) {
            this.objectConfig[attrname] = object[attrname];
        }
        return this;
    };

    this.get = function () {
        var _this = this;

        $.ajax({
            url: 'https://api.instagram.com/v1/users/' + _this.objectConfig.instagramUserId + '/media/recent?access_token=' + _this.objectConfig.instagramAccessToken + '&count=' + _this.objectConfig.quantity,
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) {
                _this.instagramReturn = data;
                _this.objectConfig.successCallback(data);
            }
        })
    };
};